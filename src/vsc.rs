pub enum VirtualScanCode {
    Invalid,
    Esc,
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12,
    E0,
    E1,
    E2,
    E3,
    E4,
    E5,
    E6,
    E7,
    E8,
    E9,
    E10,
    E11,
    E12,
    BackSpace,
    Tab,
    D1,
    D2,
    D3,
    D4,
    D5,
    D6,
    D7,
    D8,
    D9,
    D10,
    D11,
    D12,
    D13,
    CapsLock,
    C1,
    C2,
    C3,
    C4,
    C5,
    C6,
    C7,
    C8,
    C9,
    C10,
    C11,
    B0,
    B1,
    B2,
    B3,
    B4,
    B5,
    B6,
    B7,
    B8,
    B9,
    B10,
    RShift,
    A0,
    A2,
    A3,
    A4,
    LAlt,
    SpaceBar,
    LCtrl,
    RCtrl,
    SLck,
    Enter,
    DownArrow,
    LeftArrow,
    RightArrow,
    UpArrow,
    End,
    Pause,
    Print,
    PgDn,
    PgUp,
    Home,
    Ins,
    Minus,
    Plus,
    Asterisk,
    Del,
    Slash,
    NLck,
    Zero,
    One,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Period,
}

pub fn virtual_scan_code_from_u8(vsc: u8) -> Option<VirtualScanCode> {
	match vsc {
        0x00 => { Some(VirtualScanCode::Invalid) }
	    0x01 => { Some(VirtualScanCode::Esc) }
	    0x02 => { Some(VirtualScanCode::E1) }
	    0x03 => { Some(VirtualScanCode::E2) }
	    0x04 => { Some(VirtualScanCode::E3) }
	    0x05 => { Some(VirtualScanCode::E4) }
	    0x06 => { Some(VirtualScanCode::E5) }
	    0x07 => { Some(VirtualScanCode::E6) }
	    0x08 => { Some(VirtualScanCode::E7) }
	    0x09 => { Some(VirtualScanCode::E8) }
	    0x0A => { Some(VirtualScanCode::E9) }
	    0x0B => { Some(VirtualScanCode::E10) }
	    0x0C => { Some(VirtualScanCode::E11) }
	    0x0D => { Some(VirtualScanCode::E12) }
	    0x0E => { Some(VirtualScanCode::BackSpace) }
	    0x0F => { Some(VirtualScanCode::Tab) }
	    0x10 => { Some(VirtualScanCode::D1) }
	    0x11 => { Some(VirtualScanCode::D2) }
	    0x12 => { Some(VirtualScanCode::D3) }
	    0x13 => { Some(VirtualScanCode::D4) }
	    0x14 => { Some(VirtualScanCode::D5) }
	    0x15 => { Some(VirtualScanCode::D6) }
	    0x16 => { Some(VirtualScanCode::D7) }
	    0x17 => { Some(VirtualScanCode::D8) }
	    0x18 => { Some(VirtualScanCode::D9) }
	    0x19 => { Some(VirtualScanCode::D10) }
	    0x1A => { Some(VirtualScanCode::D11) }
	    0x1B => { Some(VirtualScanCode::D12) }
	    0x2B => { Some(VirtualScanCode::D13) }
	    0x1C => { Some(VirtualScanCode::Enter) }
	    0x1D => { Some(VirtualScanCode::LCtrl) }
	    0x1E => { Some(VirtualScanCode::C1) }
	    0x1F => { Some(VirtualScanCode::C2) }
	    0x20 => { Some(VirtualScanCode::C3) }
	    0x21 => { Some(VirtualScanCode::C4) }
	    0x22 => { Some(VirtualScanCode::C5) }
	    0x23 => { Some(VirtualScanCode::C6) }
	    0x24 => { Some(VirtualScanCode::C7) }
	    0x25 => { Some(VirtualScanCode::C8) }
	    0x26 => { Some(VirtualScanCode::C9) }
	    0x27 => { Some(VirtualScanCode::C10) }
	    0x28 => { Some(VirtualScanCode::C11) }
	    0x29 => { Some(VirtualScanCode::E0) }
	    0x2A => { Some(VirtualScanCode::B0) }
	    0x2c => { Some(VirtualScanCode::B1) }
	    0x2d => { Some(VirtualScanCode::B2) }
	    0x2e => { Some(VirtualScanCode::B3) }
	    0x2f => { Some(VirtualScanCode::B4) }
	    0x30 => { Some(VirtualScanCode::B5) }
	    0x31 => { Some(VirtualScanCode::B6) }
	    0x32 => { Some(VirtualScanCode::B7) }
	    0x33 => { Some(VirtualScanCode::B8) }
	    0x34 => { Some(VirtualScanCode::B9) }
	    0x35 => { Some(VirtualScanCode::B10) }
	    0x36 => { Some(VirtualScanCode::RShift) }
	    0x37 => { Some(VirtualScanCode::Asterisk) }
	    0x38 => { Some(VirtualScanCode::LAlt) }
	    0x39 => { Some(VirtualScanCode::SpaceBar) }
	    0x3A => { Some(VirtualScanCode::CapsLock) }
	    0x3B => { Some(VirtualScanCode::F1) }
	    0x3C => { Some(VirtualScanCode::F2) }
	    0x3D => { Some(VirtualScanCode::F3) }
	    0x3E => { Some(VirtualScanCode::F4) }
	    0x3F => { Some(VirtualScanCode::F5) }
	    0x40 => { Some(VirtualScanCode::F6) }
	    0x41 => { Some(VirtualScanCode::F7) }
	    0x42 => { Some(VirtualScanCode::F8) }
	    0x43 => { Some(VirtualScanCode::F9) }
	    0x44 => { Some(VirtualScanCode::F10) }
	    0x45 => { Some(VirtualScanCode::NLck) }
	    0x46 => { Some(VirtualScanCode::SLck) }
	    0x47 => { Some(VirtualScanCode::Seven) }
	    0x48 => { Some(VirtualScanCode::Eight) }
	    0x49 => { Some(VirtualScanCode::Nine) }
	    0x4A => { Some(VirtualScanCode::Minus) }
	    0x4B => { Some(VirtualScanCode::Four) }
	    0x4C => { Some(VirtualScanCode::Five) }
	    0x4D => { Some(VirtualScanCode::Six) }
	    0x4E => { Some(VirtualScanCode::Plus) }
	    0x4F => { Some(VirtualScanCode::One) }
	    0x50 => { Some(VirtualScanCode::Two) }
	    0x51 => { Some(VirtualScanCode::Three) }
	    0x52 => { Some(VirtualScanCode::Zero) }
	    0x53 => { Some(VirtualScanCode::Period) }
	    0x54 => { Some(VirtualScanCode::Print) }
	    0x56 => { Some(VirtualScanCode::B0) }
	    0x57 => { Some(VirtualScanCode::F11) }
	    0x58 => { Some(VirtualScanCode::F12) }
        _ => { None }
	}
}

pub fn virtual_scan_code_from_e0_u8(vsc: u8) -> Option<VirtualScanCode> {
	match vsc {
        0x00 => { Some(VirtualScanCode::Invalid) }
	    0x1C => { Some(VirtualScanCode::Enter) }
	    0x1D => { Some(VirtualScanCode::RCtrl) }
	    0x35 => { Some(VirtualScanCode::Slash) }
	    0x37 => { Some(VirtualScanCode::Asterisk) }
	    0x38 => { Some(VirtualScanCode::A2) }
	    0x3F => { Some(VirtualScanCode::A4) }
	    0x45 => { Some(VirtualScanCode::NLck) }
	    0x46 => { Some(VirtualScanCode::Pause) }
	    0x47 => { Some(VirtualScanCode::Home) }
	    0x48 => { Some(VirtualScanCode::UpArrow) }
	    0x49 => { Some(VirtualScanCode::PgUp) }
	    0x4B => { Some(VirtualScanCode::LeftArrow) }
	    0x4C => { Some(VirtualScanCode::UpArrow) }
	    0x4D => { Some(VirtualScanCode::RightArrow) }
	    0x4f => { Some(VirtualScanCode::End) }
	    0x50 => { Some(VirtualScanCode::DownArrow) }
	    0x51 => { Some(VirtualScanCode::PgDn) }
	    0x52 => { Some(VirtualScanCode::Ins) }
	    0x53 => { Some(VirtualScanCode::Del) }
	    0x5B => { Some(VirtualScanCode::A0) }
	    0x5C => { Some(VirtualScanCode::A3) }
        _ => { None }
	}
}

pub fn virtual_scan_code_from_e1_u8(vsc: u8) -> Option<VirtualScanCode> {
	match vsc {
        0x00 => { Some(VirtualScanCode::Invalid) }
	    0x1d => { Some(VirtualScanCode::Pause) }
        _ => { None }
	}
}
