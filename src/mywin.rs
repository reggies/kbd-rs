use std::ffi::CString;

pub type FARPROC = unsafe extern "system" fn() -> isize;
pub type HMODULE = isize;

pub unsafe fn LoadLibraryA(lplibfilename: &str) -> Option<HMODULE> {
    #[link(name = "kernel32")]
    extern "system" {
        pub fn LoadLibraryA(lplibfilename: *const u8) -> HMODULE;
    }
    let hmodule = LoadLibraryA(
        CString::new(lplibfilename)
            .expect("CString::new")
            .as_bytes_with_nul()
            .as_ptr());
    if hmodule == 0 { None } else { Some(hmodule) }
}

pub unsafe fn GetProcAddress(hmodule: HMODULE, lpprocname: &str) -> Option<FARPROC> {
    #[link(name = "kernel32")]
    extern "system" {
        pub fn GetProcAddress(hmodule: HMODULE, lpprocname: *const u8) -> Option<FARPROC>;
    }
    GetProcAddress(
        hmodule,
        CString::new(lpprocname)
            .expect("CString::new")
            .as_bytes_with_nul()
            .as_ptr())
}

pub unsafe fn FreeLibrary(hmodule: HMODULE) -> isize {
    #[link(name = "kernel32")]
    extern "system" {
        pub fn FreeLibrary(hmodule: HMODULE) -> isize;
    }
    FreeLibrary(hmodule)
}
