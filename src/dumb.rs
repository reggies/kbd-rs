#![allow(non_camel_case_types)]
#![allow(dead_code)]
#![allow(non_snake_case)]
#![allow(unused_imports)]

use std::mem::transmute;

use std::ffi::CString;
use std::collections::HashMap;

mod vk;
use vk::*;

mod mywin;
use mywin::*;

const KBDBASE: u32 = 0;
const KBDSHIFT: u32 = 1;
const KBDCTRL: u32 = 2;
const KBDALT: u32 = 4;
const KBDKANA: u32 = 8;                                   // for FE
const KBDROYA: u32 = 0x10;                                // for FE
const KBDLOYA: u32 = 0x20;                                // for FE
const KBDGRPSELTAP: u32 = 0x80;

const WCH_NONE: u16 = 0xF000;
const WCH_DEAD: u16 = 0xF001;
const WCH_LGTR: u16 = 0xF002;

const SHFT_INVALID: u8 = 0x0F;

#[repr(C)]
struct DEADKEY64 {
    dwBoth: u32,
    wchComposed: u16,
    uFlags: u16,
}

#[repr(C)]
struct VK_TO_WCHARS640 {
    VirtualKey: u8,
    Attributes: u8,
    wch: [u16; 0],
}

#[repr(C)]
struct VK_TO_WCHAR_TABLE64 {
    pVkToWchars: *const VK_TO_WCHARS640,
    nModifications: u8,
    cbSize: u8,
}

#[repr(C)]
struct VK_TO_BIT64 {
    Vk: u8,
    ModBits: u8,
}

#[repr(C)]
struct MODIFIERS640 {
    pVkToBit: *const VK_TO_BIT64,
    wMaxModBits: u16,
    ModNumber: [u8; 0],
}

#[repr(C)]
struct VSC_LPWSTR64 {
    vsc: u8,
    pwsz: *const u16,
}

#[repr(C)]
struct VSC_VK64 {
    Vsc: u8,
    Vk: u16,
}

#[repr(C)]
struct LIGATURE640 {
    VirtualKey: u8,
    ModificationNumber: u16,
    wch: [u16; 0],
}

#[repr(C)]
struct KBDTABLES64 {
    pCharMODIFIERS64: *const MODIFIERS640,
    pVkToWcharTable: *const VK_TO_WCHAR_TABLE64,
    pDEADKEY64: *const DEADKEY64,
    pKeyNames: *const VSC_LPWSTR64,
    pKeyNamesExt: *const VSC_LPWSTR64,
    pKeyNamesDead: *const *const u16,
    pusVSCtoVK: *const u16,
    bMaxVSCtoVK: u8,
    pVSCtoVK_E0: *const VSC_VK64,
    pVSCtoVK_E1: *const VSC_VK64,
    fLocaleFlags: u32,
    nLgMax: u8,
    cbLgEntry: u8,
    pLIGATURE64: *const LIGATURE640,
    dwType: u32,
    dwSubType: u32
}

type KBDLAYERDESCRIPTOR64 = unsafe extern "system" fn() -> *const KBDTABLES64;

#[derive(Debug)]
enum Wchar {
    None,
    Dead,
    Lgtr,
    IllegalCodepoint(u16),
    Normal(char)
}

#[derive(Debug)]
enum ModIndex {
    Invalid,
    Normal(u8)
}

#[derive(Debug)]
struct DeadKey64 {
    dwBoth: u32,
    wchComposed: u16,
    uFlags: u16,
}

#[derive(Debug)]
struct VkToBit64 {
    Vk: u8,
    ModBits: u8,
}

#[derive(Debug)]
struct Modifiers64 {
    pVkToBit: Vec<VkToBit64>,
    wMaxModBits: u16,
    ModNumber: Vec<ModIndex>,
}

#[derive(Debug)]
struct VkToWchars64 {
    VirtualKey: u8,
    Attributes: u8,
    wch: Vec<Wchar>,
}

#[derive(Debug)]
struct VkToWcharTable64 {
    pVkToWchars: Vec<VkToWchars64>,
    nModifications: u8,
    cbSize: u8,
}

#[derive(Debug)]
struct VscLpwstr64 {
    vsc: u8,
    pwsz: String,
}

#[derive(Debug)]
struct VscVk64 {
    Vsc: u8,
    Vk: u16,
}

#[derive(Debug)]
struct Ligature64 {
    VirtualKey: u8,
    ModificationNumber: u16,
    wch: Vec<char>,
}

#[derive(Debug)]
struct VscToVk {
    vsc: u8,
    vk: u8,
    flags: u16
}

#[derive(Debug)]
struct KbdTables64 {
    pCharMODIFIERS64: Modifiers64,
    pVkToWcharTable: Vec<VkToWcharTable64>,
    pDEADKEY64: Vec<DeadKey64>,
    pKeyNames: Vec<VscLpwstr64>,
    pKeyNamesExt: Vec<VscLpwstr64>,
    pKeyNamesDead: Vec<String>,
    pusVSCtoVK: Vec<VscToVk>,
    bMaxVSCtoVK: u8,
    pVSCtoVK_E0: Vec<VscVk64>,
    pVSCtoVK_E1: Vec<VscVk64>,
    fLocaleFlags: u32,
    nLgMax: u8,
    cbLgEntry: u8,
    pLIGATURE64: Vec<Ligature64>,
    dwType: u32,
    dwSubType: u32
}

impl ModIndex {
    fn from_raw(index: u8) -> ModIndex {
        if index == SHFT_INVALID { ModIndex::Invalid } else { ModIndex::Normal(index) }
    }
}

impl Wchar {
    fn from_raw(raw: u16) -> Wchar {
        match raw {
            WCH_NONE => {Wchar::None},
            WCH_LGTR => {Wchar::Lgtr},
            WCH_DEAD => {Wchar::Dead},
            other => {
                if let Some(ch) = std::char::from_u32(other as u32) {
                    Wchar::Normal(ch)
                } else {
                    Wchar::IllegalCodepoint(other)
                }
            }
        }
    }
}

unsafe fn parse_vk_to_bit(pVkToBit: *const VK_TO_BIT64) -> Vec<VkToBit64> {
    let mut result = Vec::new();
    let mut p = pVkToBit;
    if p != std::ptr::null() {
        while (*p).Vk != 0 {
            result.push(VkToBit64 {
                Vk: (*p).Vk,
                ModBits: (*p).ModBits,
            });
            // TBD: check alignment and panic
            p = p.offset(1);
        }
    }
    result
}

unsafe fn parse_modifiers(pCharMODIFIERS64: *const MODIFIERS640) -> Modifiers64 {
    let ModNumber = std::slice::from_raw_parts((*pCharMODIFIERS64).ModNumber.as_ptr(), (*pCharMODIFIERS64).wMaxModBits as usize)
        .iter()
        .cloned()
        .map(ModIndex::from_raw)
        .collect()
        ;
    Modifiers64 {
        pVkToBit: parse_vk_to_bit((*pCharMODIFIERS64).pVkToBit),
        wMaxModBits: (*pCharMODIFIERS64).wMaxModBits,
        ModNumber
    }
}

unsafe fn parse_vkwchar_array(nModifications: usize, cbSize: isize, pVkToWchars: *const VK_TO_WCHARS640) -> Vec<VkToWchars64> {
    let mut result = Vec::new();
    let mut p = pVkToWchars;
    while (*p).VirtualKey != 0 {
        // TBD: check nModifications against cbSize
        let wch = std::slice::from_raw_parts((*p).wch.as_ptr(), nModifications)
            .iter()
            .cloned()
            .map(Wchar::from_raw)
            .collect()
            ;
        result.push(VkToWchars64 {
            VirtualKey: (*p).VirtualKey,
            Attributes: (*p).Attributes,
            wch,
        });
        // TBD: check alignment and panic
        p = transmute::<*const u8, *const VK_TO_WCHARS640>(
            transmute::<*const VK_TO_WCHARS640, *const u8>(p)
                .offset(cbSize),
        );
    }
    result
}

unsafe fn parse_wchar_table(pVkToWchars: &VK_TO_WCHAR_TABLE64) -> VkToWcharTable64 {
    VkToWcharTable64 {
        pVkToWchars: parse_vkwchar_array(pVkToWchars.nModifications as usize, pVkToWchars.cbSize as isize, pVkToWchars.pVkToWchars),
        nModifications: pVkToWchars.nModifications,
        cbSize: pVkToWchars.cbSize
    }
}

unsafe fn parse_wchar_tables(pVkToWcharTable: *const VK_TO_WCHAR_TABLE64) -> Vec<VkToWcharTable64> {
    let mut result = Vec::new();
    let mut p : *const VK_TO_WCHAR_TABLE64 = pVkToWcharTable;
    while (*p).pVkToWchars != std::ptr::null() {
        result.push(parse_wchar_table(p.as_ref().unwrap()));
        p = p.offset(1);
    }
    result
}

unsafe fn parse_deadkey(pDEADKEY64: *const DEADKEY64) -> Vec<DeadKey64> {
    let mut result = Vec::new();
    let mut p = pDEADKEY64;
    if p != std::ptr::null() {
        while (*p).dwBoth != 0 {
            result.push(DeadKey64 {
                dwBoth: (*p).dwBoth,
                wchComposed: (*p).wchComposed,
                uFlags: (*p).uFlags
            });
            p = p.offset(1);
        }
    }
    result
}

unsafe fn parse_wstr(string: *const u16) -> Option<String> {
    if string != std::ptr::null() {
        let len = get_null_wstring_length(string);
        let raw = std::slice::from_raw_parts(string, len);
        String::from_utf16(raw).ok()
    } else {
        None
    }
}

unsafe fn parse_keynames(keynames: *const VSC_LPWSTR64) -> Vec<VscLpwstr64> {
    let mut result = Vec::new();
    let mut p = keynames;
    if p != std::ptr::null() {
        while (*p).vsc != 0 {
            if let Some(pwsz) = parse_wstr((*p).pwsz) {
                result.push(VscLpwstr64 {
                    vsc: (*p).vsc,
                    pwsz: pwsz
                });
            }
            p = p.offset(1);
        }
    }
    result
}

unsafe fn parse_wstr_array(keynames: *const *const u16) -> Vec<String> {
    let mut result = Vec::new();
    let mut p = keynames;
    if p != std::ptr::null() {
        while *p != std::ptr::null() {
            if let Some(pwsz) = parse_wstr(*p) {
                result.push(pwsz);
            }
            p = p.offset(1);
        }
    }
    result
}

unsafe fn parse_vsc_to_vk(pusVSCtoVK: *const u16, bMaxVSCtoVK: u8) -> Vec<VscToVk> {
    let mut result = Vec::new();
    for (index, item) in std::slice::from_raw_parts(pusVSCtoVK, bMaxVSCtoVK as usize).iter().cloned().enumerate() {
        result.push(VscToVk {
            vk: (item & 0xff) as u8,
            flags: item & 0xff00,
            vsc: index as u8,
        });
    }
    result
}

unsafe fn parse_vsc_to_vk_ext(pusVSCtoVK: *const VSC_VK64) -> Vec<VscVk64> {
    let mut result = Vec::new();
    let mut p = pusVSCtoVK;
    while (*p).Vk != 0 {
        result.push(VscVk64 {
            Vsc: (*p).Vsc,
            Vk: (*p).Vk,
        });
        p = p.offset(1);
    }
    result
}

unsafe fn get_null_wstring_length(string: *const u16) -> usize {
    let mut p = string;
    if p != std::ptr::null() {
        while (*p) != 0 {
            p = p.offset(1)
        }
        p.offset_from(string) as usize
    } else {
        0
    }
}

unsafe fn parse_ligatures(pLIGATURE64: *const LIGATURE640, nLgMax: u8, cbLgEntry: u8) -> Vec<Ligature64> {
    let mut result = Vec::new();
    let mut p = pLIGATURE64;
    if p != std::ptr::null() {
        while (*p).VirtualKey != 0 {
            // TBD: check nLgMax against cbLgEntry
            let wch = std::slice::from_raw_parts((*p).wch.as_ptr(), nLgMax as usize)
                .iter()
                .cloned()
                .take_while(|&wch| wch != 0 )
                .take_while(|&wch| wch != WCH_NONE )
                .flat_map(|wch| std::char::from_u32(wch as u32) )
                .collect()
                ;
            result.push(Ligature64 {
                VirtualKey: (*p).VirtualKey,
                ModificationNumber: (*p).ModificationNumber,
                wch
            });
            p = transmute::<*const u8, *const LIGATURE640>(
                transmute::<*const LIGATURE640, *const u8>(p)
                    .offset(cbLgEntry as isize),
            );
        }
    }
    result
}

unsafe fn parse_tables(tables64: *const KBDTABLES64) -> KbdTables64 {
    KbdTables64 {
        pCharMODIFIERS64: parse_modifiers((*tables64).pCharMODIFIERS64),
        pVkToWcharTable: parse_wchar_tables((*tables64).pVkToWcharTable),
        pDEADKEY64: parse_deadkey((*tables64).pDEADKEY64),
        pKeyNames: parse_keynames((*tables64).pKeyNames),
        pKeyNamesExt: parse_keynames((*tables64).pKeyNamesExt),
        pKeyNamesDead: parse_wstr_array((*tables64).pKeyNamesDead),
        pusVSCtoVK: parse_vsc_to_vk((*tables64).pusVSCtoVK, (*tables64).bMaxVSCtoVK),
        bMaxVSCtoVK: (*tables64).bMaxVSCtoVK,
        pVSCtoVK_E0: parse_vsc_to_vk_ext((*tables64).pVSCtoVK_E0),
        pVSCtoVK_E1: parse_vsc_to_vk_ext((*tables64).pVSCtoVK_E1),
        fLocaleFlags: (*tables64).fLocaleFlags,
        nLgMax: (*tables64).nLgMax,
        cbLgEntry: (*tables64).cbLgEntry,
        pLIGATURE64: parse_ligatures((*tables64).pLIGATURE64, (*tables64).nLgMax, (*tables64).cbLgEntry),
        dwType: (*tables64).dwType,
        dwSubType: (*tables64).dwSubType,
    }
}

unsafe fn print_layout(library_path: &str) {
    let module : HMODULE = LoadLibraryA(library_path)
        .expect("DLL not found");
    println!("{:?}", module);
    let proc : FARPROC = GetProcAddress(module, "KbdLayerDescriptor")
        .expect("KbdLayerDescriptor not found");
    let kbdlayers: KBDLAYERDESCRIPTOR64 = transmute(proc);
    let layers : *const KBDTABLES64 = kbdlayers();
    let tables = parse_tables(layers);
    println!("{:#?}", tables);
    FreeLibrary(module);
}

fn main() {
    unsafe { print_layout("kbda2.dll"); }
}
