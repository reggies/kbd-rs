#![allow(non_camel_case_types)]
#![allow(dead_code)]
#![allow(non_snake_case)]

// TBD: 0xD800 - 0xDB7F : High Surrogates -- not supported
// - kbdadlm.dll
// - kbdoldit.dll
// - kbdosm.dll
// - kbdgthc.dll
// - kbdosa.dll
// - kbdsora.dll

use std::mem::{transmute, size_of};
use std::ptr::addr_of;

mod vk;

mod mywin;
use mywin::*;

pub const KBDBASE: u32 = 0;
pub const KBDSHIFT: u32 = 1;
pub const KBDCTRL: u32 = 2;
pub const KBDALT: u32 = 4;
pub const KBDKANA: u32 = 8;
pub const KBDROYA: u32 = 0x10;
pub const KBDLOYA: u32 = 0x20;
pub const KBDGRPSELTAP: u32 = 0x80;

const WCH_NONE: u16 = 0xF000;
const WCH_DEAD: u16 = 0xF001;
const WCH_LGTR: u16 = 0xF002;

const SHFT_INVALID: u8 = 0x0F;

// VK_TO_WCHARS64 attributes
pub const CAPLOK: u8 = 0x01;
pub const SGCAPS: u8 = 0x02;
pub const CAPLOKALTGR: u8 = 0x04;
pub const KANALOK: u8 = 0x08;
pub const GRPSELTAP:u8 = 0x80;

// Special virtual key for
const VK_INVALID: u8 = 255;

// Attributes such as AltGr, LRM_RLM, ShiftLock are stored in the the low word
// of fLocaleFlags (layout specific) or in gdwKeyboardAttributes (all layouts)
const KLLF_ALTGR: u16 = 0x0001;
const KLLF_SHIFTLOCK: u16 = 0x0002;
const KLLF_LRM_RLM: u16 = 0x0004;

// Some attributes are per-layout (specific to an individual
// layout), some attributes are per-user (apply globally to
// all layouts).  Some are both.
const KLLF_LAYOUT_ATTRS: u16 = KLLF_SHIFTLOCK | KLLF_ALTGR | KLLF_LRM_RLM;
const KLLF_GLOBAL_ATTRS: u16 = KLLF_SHIFTLOCK;

#[derive(Debug)]
#[repr(C)]
struct DEADKEY64 {
    dwBoth: u32,
    wchComposed: u16,
    uFlags: u16,
}

#[derive(Debug)]
#[repr(C)]
struct VK_TO_WCHARS64<const N: usize> {
    VirtualKey: u8,
    Attributes: u8,
    wch: [u16; N],
}

type VK_TO_WCHARS640 = VK_TO_WCHARS64<0>;
type VK_TO_WCHARS641 = VK_TO_WCHARS64<1>;
type VK_TO_WCHARS642 = VK_TO_WCHARS64<2>;
type VK_TO_WCHARS643 = VK_TO_WCHARS64<3>;
type VK_TO_WCHARS644 = VK_TO_WCHARS64<4>;
type VK_TO_WCHARS645 = VK_TO_WCHARS64<5>;
type VK_TO_WCHARS646 = VK_TO_WCHARS64<6>;
type VK_TO_WCHARS647 = VK_TO_WCHARS64<7>;
type VK_TO_WCHARS648 = VK_TO_WCHARS64<8>;
type VK_TO_WCHARS649 = VK_TO_WCHARS64<9>;
type VK_TO_WCHARS6410 = VK_TO_WCHARS64<10>;

#[derive(Debug)]
#[repr(C)]
struct VK_TO_WCHAR_TABLE64 {
    pVkToWchars: *const VK_TO_WCHARS640,
    nModifications: u8,
    cbSize: u8,
}

#[derive(Debug)]
#[repr(C)]
struct VK_TO_BIT64 {
    Vk: u8,
    ModBits: u8,
}

#[derive(Debug)]
#[repr(C)]
struct MODIFIERS64<const N: usize> {
    pVkToBit: *const VK_TO_BIT64,
    wMaxModBits: u16,
    ModNumber: [u8; N],
}

type MODIFIERS640 = MODIFIERS64<0>;
type MODIFIERS641 = MODIFIERS64<1>;
type MODIFIERS642 = MODIFIERS64<2>;
type MODIFIERS643 = MODIFIERS64<3>;
type MODIFIERS644 = MODIFIERS64<4>;
type MODIFIERS645 = MODIFIERS64<5>;
type MODIFIERS646 = MODIFIERS64<6>;

#[derive(Debug)]
#[repr(C)]
struct VSC_LPWSTR64 {
    vsc: u8,
    pwsz: *const u16,
}

#[derive(Debug)]
#[repr(C)]
struct VSC_VK64 {
    Vsc: u8,
    Vk: u16,
}

#[derive(Debug)]
#[repr(C)]
struct LIGATURE64<const N: usize> {
    VirtualKey: u8,
    ModificationNumber: u16,
    wch: [u16; N],
}

type LIGATURE640 = LIGATURE64<0>;
type LIGATURE641 = LIGATURE64<1>;
type LIGATURE642 = LIGATURE64<2>;
type LIGATURE643 = LIGATURE64<3>;
type LIGATURE644 = LIGATURE64<4>;
type LIGATURE645 = LIGATURE64<5>;

#[derive(Debug)]
#[repr(C)]
struct KBDTABLES64 {
    pCharMODIFIERS64: *const MODIFIERS640,
    pVkToWcharTable: *const VK_TO_WCHAR_TABLE64,
    pDEADKEY64: *const DEADKEY64,
    pKeyNames: *const VSC_LPWSTR64,
    pKeyNamesExt: *const VSC_LPWSTR64,
    pKeyNamesDead: *const *const u16,
    pusVSCtoVK: *const u16,
    bMaxVSCtoVK: u8,
    pVSCtoVK_E0: *const VSC_VK64,
    pVSCtoVK_E1: *const VSC_VK64,
    fLocaleFlags: u32,
    nLgMax: u8,
    cbLgEntry: u8,
    pLIGATURE64: *const LIGATURE640,
    dwType: u32,
    dwSubType: u32
}

type KBDLAYERDESCRIPTOR64 = unsafe extern "system" fn() -> *const KBDTABLES64;

#[derive(Debug)]
pub enum Error {
    BadFile,
    BadLibrary,
    BadChar(u16),
    BadLigatureChar(u16),
    BadVkToWcharModifications(usize),
    BadVkToWcharSize(usize),
    BadKeyName(std::string::FromUtf16Error),
    BadLigatureSize(usize),
}

#[derive(PartialEq, Eq, Debug)]
pub enum Wchar {
    None,
    Dead,
    Lgtr,
    Normal(char)
}

#[derive(PartialEq, Eq, Debug)]
pub enum ModIndex {
    Invalid,
    Normal(u8)
}

#[derive(PartialEq, Eq, Debug)]
pub struct DeadKey64 {
    dwBoth: u32,
    wchComposed: u16,
    uFlags: u16,
}

#[derive(PartialEq, Eq, Debug)]
pub struct VkToBit64 {
    Vk: u8,
    ModBits: u8,
}

#[derive(PartialEq, Eq, Debug)]
pub struct Modifiers64 {
    pVkToBit: Vec<VkToBit64>,
    wMaxModBits: u16,
    ModNumber: Vec<ModIndex>,
}

#[derive(PartialEq, Eq, Debug)]
pub struct VkToWchars64 {
    VirtualKey: u8,
    Attributes: u8,
    wch: Vec<Wchar>,
}

#[derive(PartialEq, Eq, Debug)]
pub struct VkToWcharTable64 {
    pVkToWchars: Vec<VkToWchars64>,
    nModifications: u8,
    cbSize: u8,
}

#[derive(PartialEq, Eq, Debug)]
pub struct VscLpwstr64 {
    vsc: u8,
    pwsz: String,
}

#[derive(PartialEq, Eq, Debug)]
pub struct Ligature64 {
    VirtualKey: u8,
    ModificationNumber: u16,
    wch: Vec<char>,
}

#[derive(PartialEq, Eq, Debug)]
pub struct VscToVk {
    vsc: u8,
    vk: u8,
    flags: u16
}

#[derive(PartialEq, Eq, Debug)]
pub struct KbdTables64 {
    pCharMODIFIERS64: Modifiers64,
    pVkToWcharTable: Vec<VkToWcharTable64>,
    pDEADKEY64: Vec<DeadKey64>,
    pKeyNames: Vec<VscLpwstr64>,
    pKeyNamesExt: Vec<VscLpwstr64>,
    pKeyNamesDead: Vec<String>,
    pusVSCtoVK: Vec<VscToVk>,
    bMaxVSCtoVK: u8,
    pVSCtoVK_E0: Vec<VscToVk>,
    pVSCtoVK_E1: Vec<VscToVk>,
    fLocaleFlags: u32,
    nLgMax: u8,
    cbLgEntry: u8,
    pLIGATURE64: Vec<Ligature64>,
    dwType: u32,
    dwSubType: u32
}

impl ModIndex {
    fn from_raw(index: u8) -> ModIndex {
        if index == SHFT_INVALID {
            ModIndex::Invalid
        } else {
            ModIndex::Normal(index)
        }
    }
}

impl Wchar {
    fn from_raw(raw: u16) -> Result<Wchar, Error> {
        match raw {
            WCH_NONE => {Ok(Wchar::None)},
            WCH_LGTR => {Ok(Wchar::Lgtr)},
            WCH_DEAD => {Ok(Wchar::Dead)},
            other => {
                std::char::from_u32(other as u32)
                    .map(Wchar::Normal)
                    .ok_or(Error::BadChar(raw))
            }
        }
    }
}

trait Terminator {
    fn is_terminator(&self) -> bool;
}

trait AsBytes {
    fn as_bytes<'a>(&'a self) -> &'a [u8];
}

impl<T> AsBytes for T {
    fn as_bytes(&self) -> &[u8] {
        unsafe {
            std::slice::from_raw_parts(transmute(addr_of!(*self)), std::mem::size_of_val(self))
        }
    }
}

impl<T: AsBytes> Terminator for T {
    fn is_terminator(&self) -> bool {
        self.as_bytes().iter().cloned().all(|x| x == 0)
    }
}

fn parse_vk_to_bit_helper(pVkToBit: &VK_TO_BIT64) -> VkToBit64 {
    VkToBit64 {
        Vk: pVkToBit.Vk,
        ModBits: pVkToBit.ModBits,
    }
}

unsafe fn parse_vk_to_bit(pVkToBit: *const VK_TO_BIT64) -> Vec<VkToBit64> {
    parse_slice_null_terminated(pVkToBit)
        .iter()
        .map(parse_vk_to_bit_helper)
        .collect()
}

unsafe fn parse_modifiers(pCharMODIFIERS64: *const MODIFIERS640) -> Result<Modifiers64, Error> {
    pCharMODIFIERS64
        .as_ref()
        .ok_or(Error::BadLibrary)
        .map(|pCharMODIFIERS64| Modifiers64 {
            pVkToBit: parse_vk_to_bit((*pCharMODIFIERS64).pVkToBit),
            wMaxModBits: (*pCharMODIFIERS64).wMaxModBits,
            ModNumber: std::slice::from_raw_parts((*pCharMODIFIERS64).ModNumber.as_ptr(), (*pCharMODIFIERS64).wMaxModBits as usize)
                .iter()
                .cloned()
                .map(ModIndex::from_raw)
                .collect()
        })
}

fn parse_vk_to_wchar_table_helper<const N: usize>(pVkToWchars: &VK_TO_WCHARS64<N>, nModifications: usize) -> Result<VkToWchars64, Error> {
    if nModifications <= N {
        pVkToWchars.wch
            .iter()
            .cloned()
            .map(Wchar::from_raw)
            .collect::<Result<Vec<Wchar>, Error>>()
            .map(|wch| VkToWchars64 {
                VirtualKey: pVkToWchars.VirtualKey,
                Attributes: pVkToWchars.Attributes,
                wch
            })
    } else {
        Err(Error::BadVkToWcharSize(nModifications))
    }
}

unsafe fn parse_vk_to_wchars(nModifications: usize, cbSize: isize, pVkToWchars: *const VK_TO_WCHARS640) -> Result<Vec<VkToWchars64>, Error> {
    unsafe fn helper<const N: usize>(pVkToWchars: *const VK_TO_WCHARS640, nModifications: usize) -> Result<Vec<VkToWchars64>, Error> {
        parse_slice_null_terminated(transmute::<_, *const VK_TO_WCHARS64<N>>(pVkToWchars))
            .iter()
            .map(|p| parse_vk_to_wchar_table_helper(p, nModifications))
            .collect()
    }

    match cbSize as usize {
        0 => {Ok(Vec::new())}
        x if x == size_of::<VK_TO_WCHARS640>() => {helper::<0>(pVkToWchars, nModifications)}
        x if x == size_of::<VK_TO_WCHARS641>() => {helper::<1>(pVkToWchars, nModifications)}
        x if x == size_of::<VK_TO_WCHARS642>() => {helper::<2>(pVkToWchars, nModifications)}
        x if x == size_of::<VK_TO_WCHARS643>() => {helper::<3>(pVkToWchars, nModifications)}
        x if x == size_of::<VK_TO_WCHARS644>() => {helper::<4>(pVkToWchars, nModifications)}
        x if x == size_of::<VK_TO_WCHARS645>() => {helper::<5>(pVkToWchars, nModifications)}
        x if x == size_of::<VK_TO_WCHARS646>() => {helper::<6>(pVkToWchars, nModifications)}
        x if x == size_of::<VK_TO_WCHARS647>() => {helper::<7>(pVkToWchars, nModifications)}
        x if x == size_of::<VK_TO_WCHARS648>() => {helper::<8>(pVkToWchars, nModifications)}
        x if x == size_of::<VK_TO_WCHARS649>() => {helper::<9>(pVkToWchars, nModifications)}
        x if x == size_of::<VK_TO_WCHARS6410>() => {helper::<10>(pVkToWchars, nModifications)}
        _ => {Err(Error::BadVkToWcharSize(cbSize as usize))}
    }
}

unsafe fn parse_vk_to_wchar_table(pVkToWchars: &VK_TO_WCHAR_TABLE64) -> Result<VkToWcharTable64, Error> {
    parse_vk_to_wchars(pVkToWchars.nModifications as usize, pVkToWchars.cbSize as isize, pVkToWchars.pVkToWchars)
        .map(|wchars| VkToWcharTable64 {
            pVkToWchars: wchars,
            nModifications: pVkToWchars.nModifications,
            cbSize: pVkToWchars.cbSize
        })
}

unsafe fn parse_vk_to_wchar_tables(pVkToWcharTable: *const VK_TO_WCHAR_TABLE64) -> Result<Vec<VkToWcharTable64>, Error> {
    parse_slice_null_terminated(pVkToWcharTable)
        .iter()
        .map(|c| parse_vk_to_wchar_table(c))
        .collect()
}

fn parse_deadkey_helper(pDEADKEY64: &DEADKEY64) -> DeadKey64 {
    DeadKey64 {
        dwBoth: pDEADKEY64.dwBoth,
        wchComposed: pDEADKEY64.wchComposed,
        uFlags: pDEADKEY64.uFlags
    }
}

unsafe fn parse_deadkey(pDEADKEY64: *const DEADKEY64) -> Vec<DeadKey64> {
    parse_slice_null_terminated(pDEADKEY64)
        .iter()
        .map(parse_deadkey_helper)
        .collect()
}

unsafe fn parse_wstr(string: *const u16) -> Result<String, Error> {
    if string == std::ptr::null() {
        Ok(From::from(""))
    } else {
        String::from_utf16(parse_slice_null_terminated(string))
            .map_err(Error::BadKeyName)
    }
}

unsafe fn parse_vsc_lpwstr64_helper(keynames: &VSC_LPWSTR64) -> Result<VscLpwstr64, Error> {
    parse_wstr(keynames.pwsz)
        .map(|pwsz| VscLpwstr64 {
            vsc: keynames.vsc,
            pwsz: pwsz
        })
}

unsafe fn parse_keynames(keynames: *const VSC_LPWSTR64) -> Result<Vec<VscLpwstr64>, Error> {
    parse_slice_null_terminated(keynames)
        .iter()
        .map(|c| parse_vsc_lpwstr64_helper(c))
        .collect()
}

unsafe fn parse_wstr_array(keynames: *const *const u16) -> Result<Vec<String>, Error> {
    parse_slice_null_terminated(keynames)
        .iter()
        .map(|&c| parse_wstr(c))
        .collect()
}

unsafe fn parse_vsc_to_vk(pusVSCtoVK: *const u16, bMaxVSCtoVK: u8) -> Vec<VscToVk> {
    std::slice::from_raw_parts(pusVSCtoVK, bMaxVSCtoVK as usize)
        .iter()
        .cloned()
        .enumerate()
        .map(|(index, item)| VscToVk {
            vk: (item & 0xff) as u8,
            flags: item & 0xff00,
            vsc: index as u8,
        })
        .collect()
}

fn parse_vsc_to_vk_helper(pusVSCtoVK: &VSC_VK64) -> VscToVk {
    VscToVk {
        vsc: pusVSCtoVK.Vsc,
        flags: (pusVSCtoVK.Vk & 0xff00) as u16,
        vk: (pusVSCtoVK.Vk & 0xff) as u8,
    }
}

unsafe fn parse_vsc_to_vk_ext(pusVSCtoVK: *const VSC_VK64) -> Vec<VscToVk> {
    parse_slice_null_terminated(pusVSCtoVK)
        .iter()
        .map(parse_vsc_to_vk_helper)
        .collect()
}

unsafe fn parse_slice_null_terminated<'a, T: Terminator>(src: *const T) -> &'a [T] {
    let mut p = src;
    let mut count = 0;
    if p != std::ptr::null() {
        while !(*p).is_terminator() {
            count += 1;
            p = p.offset(1);
        }
        std::slice::from_raw_parts(src, count)
    } else {
        &[]
    }
}

fn parse_ligature_wchar(wc: u16) -> Result<char, Error> {
    if wc == 0 || wc == WCH_NONE || wc == WCH_LGTR || wc == WCH_DEAD {
        Err(Error::BadLigatureChar(wc))
    } else {
        std::char::from_u32(wc as u32)
            .ok_or(Error::BadLigatureChar(wc))
    }
}

fn parse_ligature_helper<const N: usize>(pLIGATURE64: &LIGATURE64<N>) -> Result<Ligature64, Error> {
    pLIGATURE64.wch
        .iter()
        .cloned()
        .take_while(|&wc| wc != 0 )
        .take_while(|&wc| wc != WCH_NONE )
        .map(parse_ligature_wchar)
        .collect::<Result<Vec<char>, Error>>()
        .map(|wch| Ligature64 {
            VirtualKey: pLIGATURE64.VirtualKey,
            ModificationNumber: pLIGATURE64.ModificationNumber,
            wch
        })
}

unsafe fn parse_ligatures(pLIGATURE64: *const LIGATURE640, cbLgEntry: u8) -> Result<Vec<Ligature64>, Error> {
    unsafe fn helper<const N: usize>(pLIGATURE64: *const LIGATURE640) -> Result<Vec<Ligature64>, Error> {
        parse_slice_null_terminated(transmute::<_, *const LIGATURE64<N>>(pLIGATURE64))
            .iter()
            .map(parse_ligature_helper)
            .collect()
    }
    match cbLgEntry as usize {
        x if x == size_of::<LIGATURE640>() => {helper::<0>(pLIGATURE64)}
        x if x == size_of::<LIGATURE641>() => {helper::<1>(pLIGATURE64)}
        x if x == size_of::<LIGATURE642>() => {helper::<2>(pLIGATURE64)}
        x if x == size_of::<LIGATURE643>() => {helper::<3>(pLIGATURE64)}
        x if x == size_of::<LIGATURE644>() => {helper::<4>(pLIGATURE64)}
        x if x == size_of::<LIGATURE645>() => {helper::<5>(pLIGATURE64)}
        0 => {Ok(Vec::new())}
        _ => {Err(Error::BadLigatureSize(cbLgEntry as usize))}
    }
}

unsafe fn parse_tables(tables64: *const KBDTABLES64) -> Result<KbdTables64, Error> {
    let tables64            = tables64.as_ref().ok_or(Error::BadLibrary)?;
    let pCharMODIFIERS64    = parse_modifiers(tables64.pCharMODIFIERS64)?;
    let pVkToWcharTable     = parse_vk_to_wchar_tables(tables64.pVkToWcharTable)?;
    let pKeyNames           = parse_keynames(tables64.pKeyNames)?;
    let pKeyNamesExt        = parse_keynames(tables64.pKeyNamesExt)?;
    let pKeyNamesDead       = parse_wstr_array(tables64.pKeyNamesDead)?;
    let pLIGATURE64         = parse_ligatures(tables64.pLIGATURE64, tables64.cbLgEntry)?;
    Ok(KbdTables64 {
        pCharMODIFIERS64,
        pVkToWcharTable,
        pDEADKEY64: parse_deadkey(tables64.pDEADKEY64),
        pKeyNames,
        pKeyNamesExt,
        pKeyNamesDead,
        pusVSCtoVK: parse_vsc_to_vk(tables64.pusVSCtoVK, tables64.bMaxVSCtoVK),
        pVSCtoVK_E0: parse_vsc_to_vk_ext(tables64.pVSCtoVK_E0),
        pVSCtoVK_E1: parse_vsc_to_vk_ext(tables64.pVSCtoVK_E1),
        pLIGATURE64,
        bMaxVSCtoVK: tables64.bMaxVSCtoVK,
        fLocaleFlags: tables64.fLocaleFlags,
        nLgMax: tables64.nLgMax,
        cbLgEntry: tables64.cbLgEntry,
        dwType: tables64.dwType,
        dwSubType: tables64.dwSubType,
    })
}

struct Module {
    module: HMODULE
}

impl Module {
    fn new(libname: &str) -> Option<Module> {
        unsafe { LoadLibraryA(libname).map(|module| Module {module}) }
    }
}

impl Drop for Module {
    fn drop(&mut self) {
        unsafe { FreeLibrary(self.module); }
    }
}

pub unsafe fn load_layout(library_path: &str) -> Result<KbdTables64, Error> {
    let module = Module::new(library_path)
        .ok_or(Error::BadFile)?;
    let proc : FARPROC = GetProcAddress(module.module, "KbdLayerDescriptor")
        .ok_or(Error::BadLibrary)?;
    let kbdlayers: KBDLAYERDESCRIPTOR64 = transmute(proc);
    parse_tables(kbdlayers())
}

#[cfg(test)]
mod test {
    use crate::*;
    use crate::vk::*;

    fn load_layout_safe(library_path: &str) -> Result<KbdTables64, Error> {
        unsafe {load_layout(library_path)}
    }

    fn test_invalid_virtual_keys_and_attributes(tables: &KbdTables64) {
        for wch_table in tables.pVkToWcharTable.iter() {
            let pairs = wch_table.pVkToWchars.iter().zip(wch_table.pVkToWchars.iter().skip(1));
            for (vk_to_wch1, vk_to_wch2) in pairs {
                if vk_to_wch2.VirtualKey == VK_INVALID {
                    assert!(vk_to_wch1.Attributes == SGCAPS,
                            "{:?} has invalid attributes", vk_to_wch1);
                }
            }
        }
    }

    fn test_no_unknown_virtual_keys(tables: &KbdTables64) {
        for vsc_to_vk in tables.pusVSCtoVK.iter() {
            assert!(valid_virtual_key(vsc_to_vk.vk) ||
                        vsc_to_vk.vk == VK_INVALID,
                    "{:?} has invalid virtual key", vsc_to_vk);
        }
        for vsc_to_vk in tables.pVSCtoVK_E0.iter() {
            assert!(valid_virtual_key(vsc_to_vk.vk) ||
                        vsc_to_vk.vk == VK_INVALID,
                    "{:?} has invalid virtual key", vsc_to_vk);
        }
        for vsc_to_vk in tables.pVSCtoVK_E1.iter() {
            assert!(valid_virtual_key(vsc_to_vk.vk) ||
                        vsc_to_vk.vk == VK_INVALID,
                    "{:?} has invalid virtual key", vsc_to_vk);
        }
        for wch_table in tables.pVkToWcharTable.iter() {
            for vk_to_wch in wch_table.pVkToWchars.iter() {
                assert!(valid_virtual_key(vk_to_wch.VirtualKey) ||
                        vk_to_wch.VirtualKey == VK_INVALID,
                        "{:?} has invalid virtual key", vk_to_wch);
            }
        }
    }

    fn test_layout(libname: &str) -> Result<KbdTables64, Error> {
        let tables = load_layout_safe(libname)?;
        test_no_unknown_virtual_keys(&tables);
        test_no_unknown_virtual_keys(&tables);
        Ok(tables)
    }

    #[test]
    fn test_as_bytes() {
        let n : u32 = 32;
        let n_bytes = n.as_bytes();
        assert!(n_bytes == &[32, 0, 0, 0]);

        let s = "hello";
        let s_bytes = s.as_bytes();
        assert!(s_bytes == &[104, 101, 108, 108, 111]);
    }

    #[test]
    fn test_kbdus() {
        test_layout("./layouts/kbdus.dll").expect("ok");
    }

    #[test]
    fn test_kbd101() {
        test_layout("./layouts/kbd101.dll").expect("ok");
    }

    #[test]
    fn test_kbdcz() {
        let tables = test_layout("./layouts/kbdcz.dll").expect("ok");
        println!("{:#?}", tables);
    }

    #[test]
    fn test_arabic_102() {
        test_layout("./layouts/kbda2.dll").expect("ok");
    }

    use std::io;
    use std::fs;

    #[test]
    fn test_all() -> io::Result<()> {
        for entry in fs::read_dir("./layouts/")? {
            let entry = entry?;
            let path = entry.path();
            if path.is_file() {
                if path.extension().expect("only files with extension") == "dll" {
                    println!("testing {:?}", path);
                    test_layout(path.to_str().expect("proper utf8"))
                        .expect("ok");
                }
            }
        }
        Ok(())
    }
}
